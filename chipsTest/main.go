package chipsTest

import (
	"chips/baseChips"
	"chips/chips"
	"fmt"
)

var (
	P     = fmt.Println
	Trans = chips.TransIntToBool
)

func TestNor() {
	_, _ = P(chips.Nor(Trans(0), Trans(0)))
	_, _ = P(chips.Nor(Trans(0), Trans(1)))
	_, _ = P(chips.Nor(Trans(1), Trans(0)))
	_, _ = P(chips.Nor(Trans(1), Trans(1)))
}

func TestDMux8Way() {
	fmt.Println(baseChips.DMux8Way(true, [3]bool{false, false, false})) //true false false false false false false false
	fmt.Println(baseChips.DMux8Way(true, [3]bool{true, false, false}))  //false true false false false false false false
	fmt.Println(baseChips.DMux8Way(true, [3]bool{false, true, false}))  //false false true false false false false false
}

func TestFullAdder() {
	_, _ = P(chips.FullAdder(Trans(0), Trans(0), Trans(0)))
	_, _ = P(chips.FullAdder(Trans(0), Trans(0), Trans(1)))
	_, _ = P(chips.FullAdder(Trans(1), Trans(1), Trans(0)))
	_, _ = P(chips.FullAdder(Trans(1), Trans(1), Trans(1)))
}

func TestFourBitAdder() {
	var (
		a = []bool{Trans(1), Trans(1), Trans(0), Trans(1)}
		b = []bool{Trans(0), Trans(0), Trans(0), Trans(1)}
	)
	fmt.Println(chips.FourBitAdder(a[0], b[0], a[1], b[1], a[2], b[2], a[3], b[3]))
}

func TestBit8Adder() {
	fmt.Println(chips.Bit8Adder([8]bool{true, true}, [8]bool{true}))
	fmt.Println(chips.Bit8Adder([8]bool{true}, [8]bool{true}))
}

func TestFF() {
	ff := chips.FF()
	println(ff(false, false))
	println(ff(true, false))
	println(ff(false, false))
	println(ff(false, true))
}

func TestDFF() {
	dff := chips.DFF()
	fmt.Println(dff(true, false))  // FALSE
	fmt.Println(dff(true, true))   // true
	fmt.Println(dff(true, false))  // true
	fmt.Println(dff(false, false)) //true
	fmt.Println(dff(false, true))  //false
}

func TestEightDFF() {
	eightDFF := chips.EightDFF()
	fmt.Println(eightDFF([8]bool{false, false, true, false, true, false, false, true}, true))
}

func TestRegister() {
	register := chips.Register()
	fmt.Println(register([16]bool{true, false, false, false, false, false, false, true, false, false, false, false, false, false, false, true}, true))
	fmt.Println(register([16]bool{true, false, false, true, true, false, false, true, false, false, false, false, false, false, false, true}, false)) //同上
}

func TestMux16() {
	fmt.Println(baseChips.Mux16(
		[16]bool{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true},
		[16]bool{false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false},
		true,
	)) // [false, false, false, false, false, false, false, false,false, false, false, false, false, false, true, false] 选择b

	fmt.Println(baseChips.Mux16(
		[16]bool{false, false, false, false, false, false, false, false, false, false, false, false, false, true, false, true},
		[16]bool{false, false, false, false, false, false, false, false, false, false, false, false, false, false, true, false},
		false,
	)) // [false, false, false, false, false, false, false, false,false, false, false, false, false, true, false, true] 选择a
}

func TestMux() {
	fmt.Println(baseChips.Mux(true, true, false))
}

func TestOneToTwo() {
	fmt.Println(chips.OneToTwo(false, true))
}

func TestThreeWayToOne() {
	point := [2]bool{true, false}
	point2 := [2]bool{false, false}
	point3 := [2]bool{false, true}
	fmt.Println(chips.ThreeWayToOne(point, true, false, false))  //true
	fmt.Println(chips.ThreeWayToOne(point2, false, false, true)) //true
	fmt.Println(chips.ThreeWayToOne(point3, false, true, false)) //true
}

func TestThreeWayToEight() {
	fmt.Println(chips.ThreeWayToEight(false, false, false, true)) // [1,0,0,0,0,0,0,0]
	fmt.Println(chips.ThreeWayToEight(false, false, true, true))  // [0,1,0,0,0,0,0,0]
}

func TestMem() {
	mem := chips.Mem()
	mem([3]bool{false, false, false}, true, true) // t
	mem([3]bool{false, false, true}, true, true)  // [1,1,0,0,0,0,0,0]
	mem([3]bool{false, false, true}, true, false) // [1,0,0,0,0,0,0,0]
}

func TestNot16() {
	fmt.Println(baseChips.Not16([16]bool{true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false}))
}

func TestBit16Adder() {
	fmt.Println(chips.Bit16Adder(
		[16]bool{true, true, true, false, false, false, false, false, false, false, false, false, false, false, false, false},
		[16]bool{false, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
	))
}

func TestPC() {
	pc := chips.PC()
	o1 := pc([16]bool{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false}, false, true, false)
	o2 := pc([16]bool{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false}, false, true, false)
	o3 := pc([16]bool{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false}, false, false, false)
	o4 := pc([16]bool{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true}, true, false, false)
	o5 := pc([16]bool{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false}, false, true, false)
	o6 := pc([16]bool{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false}, false, false, true)

	fmt.Println(o1)
	fmt.Println(o2)
	fmt.Println(o3)
	fmt.Println(o4)
	fmt.Println(o5)
	fmt.Println(o6)
}

func TestRAM8() {
	ram8 := chips.RAM8()
	fmt.Println(
		ram8(
			[16]bool{true, true, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
			true,
			[3]bool{true, false, false},
		),
	)

	fmt.Println(
		ram8(
			[16]bool{false, true, false, true, true, false, false, false, false, false, false, false, false, false, false, false},
			true,
			[3]bool{true, true, false},
		),
	) //写入3
	fmt.Println(ram8([16]bool{}, false, [3]bool{true, false, false})) //读取 3
	fmt.Println(ram8([16]bool{}, false, [3]bool{true, true, false}))  //读取 3
}

func TestRAM64() {
	ram64 := chips.RAM64()
	fmt.Println(
		ram64(
			[16]bool{true},
			true,
			[6]bool{},
		),
	)

	fmt.Println(
		ram64(
			[16]bool{true, false, true},
			true,
			[6]bool{true},
		),
	) //写入3
	fmt.Println(ram64([16]bool{}, false, [6]bool{}))     //读取 3
	fmt.Println(ram64([16]bool{}, false, [6]bool{true})) //读取 3
}

func TestSimplePc() {
	simplePc := chips.SimplePc()
	simplePc()
	simplePc()
	simplePc()
}
